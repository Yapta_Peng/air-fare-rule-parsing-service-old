package com.yapta.air.farerule.parsing.api.v1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/health")
public class HealthController {

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity healthCheck() {

        return new ResponseEntity(HttpStatus.OK);
    }
}